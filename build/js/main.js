$(document).ready(function() {
    $('select').selectbox();

    $(".selectbox li:first").remove();

    _.each($('.selectbox li'), function  (key, i) {
        var value = $(key).text();
        $(key)
            .empty()
            .append('<span>'+ value +'</span>');
    });

    var $caseColorDiv = $('.caseColor');
    var caseImg = $('.case').get(0);
    var $caseTextContainer = $('.caseTextContainer');
    var $caseTextInput = $('.caseTextInput');
    var currentCaseColor = 'black';

    $('.caseColor').on('click', function(event) {
        $caseColorDiv.each(function() {
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
        currentCaseColor = $(this).attr('data-value');
        changeCaseColor(currentCaseColor);
        getImage($caseTextInput.val());
    });

    var checkFieldsResult = false;
    $('.sendEmailBtn').on('click', function() {
        checkFieldsResult = sendEmail();
    });

    $('#modal-checkout').on('hidden.bs.modal', function() {
        sendMailCallBack(checkFieldsResult);
    });



    $caseTextInput.on('keyup', function() {
        var self = this;
        getImage($(self).val());
    });

    function sendMailCallBack(result) {
        if (result.success) {
            $("#modal-success").modal();
        }
        else {
            var text = result.errors.join('<br>');
            $("#modal-error .errorsDescriptions").html(text);
            $("#modal-error").modal();
        }
    }

    function getImage(text) {
        var color = {
            "white": "white",
            "black": "black",
            "green": "black",
            "pink": "black",
            "red": "black"
        };

        var imageUrl = "http://kotiki.part-web.net/maket/img?text=" +
            encodeURIComponent(text) + "&color=" + color[currentCaseColor];
        $caseTextContainer.html("<img src='" + imageUrl + "'align='<center></center>'>");
    }

    function changeCaseColor(color) {
        caseImg.src = 'img/top_' + color + '.png';
    }

    $('.slider').slick({
        infinite: true,
        // variableWidth: true,
        arrows: false,
        slidesToShow: 7,
        pauseOnHover: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 500,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    });

    function sendEmail() {
        var required = {
            model: $('.customerModelSelect').val(),
            name: $('.customerName').val(),
            bgcolor: $('.caseColor.selected').attr('data-value'),
            text: $('.caseTextInput').val(),
            phone: $('.customerPhone').val(),
            contact: $('.customerEmail').val(),
        };

        var errorsDescription = {
            model: 'Необходимо выбрать модель',
            name: 'Необходимо ввести имя',
            text: 'Необходимо ввести текст',
            phone: 'Необходимо ввести номер',
            contact: 'Необходимо ввести почтовый адрес или ссылку на страницу в ВК',
        };

        var errors = [];
        for (var key in required) {
            if (!required[key]) {
                errors.push(errorsDescription[key]);
            }
        }

        var comment = $('.customerComment').val();
        if (comment) {
            required.comment = comment;
        }

        if (errors.length) {
            return {success: false, errors: errors};
        }
        else {
            var url = "http://herman-email.herokuapp.com/mail?" + $.param(required);
            $.get(url)
            .success(function(res) {console.log(res)})
            .error(function(res) {console.log(res)})
            return {success: true};
        }
    }

    $('.slider').show();
});
